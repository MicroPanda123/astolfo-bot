import { serve } from "https://deno.land/std@0.131.0/http/server.ts";
// Sift is a library that has functions that makes it easier to validate requests and make json responses.
import { json, validateRequest } from "https://deno.land/x/sift@0.4.1/mod.ts";
// TweetNaCl is a cryptography library that allows to verify discord requests.
import nacl from "https://cdn.skypack.dev/tweetnacl@v1.0.3?dts";
// Supabase is a library that allows to fetch data from supabase API's.
import { createClient } from "https://esm.sh/@supabase/supabase-js@1.35.4";

// supabaseClient is used to fetch data from supabase database.
const supabaseClient = createClient(
  // Supabase variables are automatically in env on deploy.
  Deno.env.get("SUPABASE_URL") ?? "",
  Deno.env.get("SUPABASE_ANON_KEY") ?? ""
);

serve(async (request: Request) => {
  // validateRequest ensures that a request is of POST method and has the required headers.
  const { error } = await validateRequest(request, {
    POST: {
      headers: ["X-Signature-Ed25519", "X-Signature-Timestamp"],
    },
  });
  if (error) {
    return json({ error: error.message }, { status: error.status });
  }

  // verifySignature verifies if the request is coming from Discord.ests to test our verification.
  const { valid, body } = await verifySignature(request);
  // When the request's signature is not valid, we return a 401 and this is important as Discord sends invalid requ
  if (!valid) {
    return json(
      { error: "Invalid request" },
      {
        status: 401,
      }
    );
  }
  // console.log(body);
  const { type = 0, data = { options: [] }, channel_id } = JSON.parse(body);
  // console.log(data.options[0].options[0]);
// try {  

  // Discord performs Ping interactions to test our application.
  // Type 1 in a request means it's ping interaction.
  if (type === 1) {
    return json({
      type: 1, // Type 1 in a response is a Pong interaction response.
    });
  }

  // Type 2 in a request is an ApplicationCommand interaction.
  // It means it's user made interaction
  if (type === 2) {
    const option = data.options[0].name;
    let id: number | null = null;
    let astolfo_rating: string = "safe";
    let nsfw_out: boolean = false;
    if (data.options[0].options[0]) {
      if (data.options[0].options[0].name === 'id') {
        id = data.options[0].options[0].value; 
      }
      
      if (data.options[0].options[0].name === 'nsfw') {
        const response = await fetch("https://discord.com/api/v10/channels/" + channel_id, { headers: { Authorization: 'Bot '+ Deno.env.get("DISCORD_BOT_TOKEN"),} });
        const { nsfw } = await response.json();
        nsfw_out = nsfw;
        if (nsfw){
          if (data.options[0].options[0].value == true){
            astolfo_rating = "explicit";
          }
        }
        else {
          return json({
            type: 4,
            data: {
              content: `Put your dick away walter, this is NOT a NSFW channel.`,
            },
          })
        }
      }
    }
    // } catch (error) {
      // console.log(error);
    // }
    // console.log(nsfw);
    // In the following:
    // if(option === " ... ") check what interaction user choose, i.e. random
    // supabaseClient is used to fetch data from database
    // idToURL function turns id fetched from supabase database to image URL (currently source is astolfo.rocks)
    // Type 4 response means it's response to iteraction with message
    if (option === "random") {
      const astolfo = await supabaseClient
        .from("random_astolfo")
        .select("id,file_extension,url")
        .eq("rating", astolfo_rating)
        .limit(1)
        .single();
      const data = {
        type: 4,
        data: {
          content: `${astolfo.data.url}`,
        },
      };
      return json(data);
    }
    if (option === "select") {
      const astolfo = await supabaseClient
        .from("astolfo")
        .select("id,file_extension,rating,url")
        .eq("id", id)
        .single();
      if (astolfo.data) {
        if (astolfo.data.rating === "safe" || nsfw_out) {
          return json({
            type: 4,
            data: {
              content: `${astolfo.data.url}`,
            },
          });
        }
        return json({
          type: 4,
          data: {
            content: `Put your dick away walter, this is NOT a NSFW channel.`,
          },
        });
      }
      return json({
        type: 4,
        data: {
          content: `Picture with given ID doesn't exist.`,
        },
      });
    }
    return json({
      type: 4,
      data: {
        content: `empty`,
      },
    });
  }

  // We will return a bad request error as a valid Discord request shouldn't reach here.
  return json({ error: "bad request" }, { status: 400 });
});

// Verify whether the request is coming from Discord.
async function verifySignature(
  request: Request
): Promise<{ valid: boolean; body: string }> {
  // You have to provide this key.
  const PUBLIC_KEY = Deno.env.get("DISCORD_PUBLIC_KEY")!;
  // Discord sends these headers with every request.
  const signature = request.headers.get("X-Signature-Ed25519")!;
  const timestamp = request.headers.get("X-Signature-Timestamp")!;
  const body = await request.text();
  const valid = nacl.sign.detached.verify(
    new TextEncoder().encode(timestamp + body),
    hexToUint8Array(signature),
    hexToUint8Array(PUBLIC_KEY)
  );

  return { valid, body };
}

// Converts a hexadecimal string to Uint8Array. 
function hexToUint8Array(hex: string) {
  return new Uint8Array(hex.match(/.{1,2}/g)!.map((val) => parseInt(val, 16)));
}